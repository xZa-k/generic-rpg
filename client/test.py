from math import floor, ceil
HEIGHT = 15
WIDTH = 39


def chunkToArrayPos(x: int, y: int):
    return (x + WIDTH//2, -(y - HEIGHT//2))

assert chunkToArrayPos(0,0) == (19, 7) # Tests if it has correct zeroing
assert chunkToArrayPos(-19, 7) == (0,0)  # Reverse test if it is relative to top left corner

print("Passed first test")

def centerToTopLeft(x: int, y: int):
    return (x - WIDTH//2, -(y - HEIGHT//2))

assert centerToTopLeft(0,0) == (-19, 7) # Tests if it has correct zeroing
assert centerToTopLeft(19, 7) == (0,0)  # Reverse test if it is relative to top left corner

print("Passed second test")


def worldToChunkDelta(x: int, y: int) :
        return (x - WIDTH*round(x / WIDTH), (y - HEIGHT*round(y / HEIGHT)))

assert worldToChunkDelta(0, 0) == (0, 0) # Tests for correct zeroing
assert worldToChunkDelta(-39, 0) == (0, 0) # Tests if it is zeroed for other world chunks
assert worldToChunkDelta(-40, 1) == (-1, 1) # Tests if it returns the correct delta from the chunk
assert worldToChunkDelta(156, 60) == (0, 0) # Tests if distant chunks are correctly zeroed

print("Passed third test")


def worldToChunk(x: int, y: int):
        return (round(x / WIDTH), round(y / HEIGHT))

assert worldToChunk(0, 0) == (0, 0) # Tests if zeroed correctly
assert worldToChunk(19, 7) == (0, 0) # Tests if border coordinates are still in correct chunk
assert worldToChunk(156, 60) == (4, 4) # Tests distant chunks

print("Passed fourth test")


# def chunkCenter(x, y):
#     return (round(39/2 + x), -round(15/2 + y))

# def centerToArray(x, y):
#     print(x + 39/2)
#     return round(x + 39/2)

# print(chunkCenter(1, 6))
# print(centerToArray(0, 0))



# def worldToChunk(x, y):
#     return (x - 39*round(x / 39), -(y - 15*round(y / 15)))
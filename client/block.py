

class Tile:
    def __init__(self, symbol="?",bg="Black",fg="White", collision=False):
        self.props = {
            "symbol": symbol,
            "bg":bg,
            "fg":fg,
            "collision":collision
        }

    @property
    def symbol(self):
        return self.props["symbol"]

    @property
    def bg(self):
        return self.props["bg"]

    @property
    def fg(self):
        return self.props["fg"]

    @property
    def collision(self):
        return self.props["collision"]

    def interact(*args):
        raise NotImplementedError

    def __str__(self):
        return self.symbol

class Grass(Tile):
    def __init__(self):
        symbol=" "
        bg="Green"
        fg="Green"
        collision=False
        super().__init__(symbol,bg,fg,collision)

class StoneWall(Tile):
    def __init__(self):
        symbol="#"
        bg="Gray"
        fg="Black"
        collision=True
        super().__init__(symbol,bg,fg,collision)

class Tree(Tile):
    def __init__(self):
        symbol="⯭"
        bg="Brown"
        fg="Green"
        collision=True
        super().__init__(symbol,bg,fg,collision)

class Water(Tile):
    def __init__(self):
        symbol=" "
        bg="Blue"
        fg="Blue"
        collision=False
        super().__init__(symbol,bg,fg,collision)

if __name__ == "__main__":
    tile = Tree()
    print(tile.fg)


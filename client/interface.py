from util import *
from copy import deepcopy
class Interface:
    
    def __init__(self, height, width):
        self.components = []
        self.height = height
        self.width = width
        self._content = [[" " for x in range(self.width)] for y in range(self.height)] # Creates a blank interface
    
    @property
    def content(self):
        return self._content

    @property
    def dimensions(self):
        return Vec2(self.height, self.width)

    #To easily add components to the interface
    def __iadd__(self, other):
        if not isinstance(other, Component):
            raise ValueError("Can only add component instances to interface")
        self.components.append(other)
        return self

    def display(self):
        #Loops through components, calling the display function
        interfaceContent = deepcopy(self.content)
        for component in self.components:
            Component.overlap(interfaceContent, component.content, component.pos)
        for y in range(self.height):
            for x in range(self.width):
                print(interfaceContent[y][x], end="")
            print()

class Component:
    
    def __init__(self, pos):
        self.x = pos.x
        self.y = pos.y
        self._content = []

    @property
    def pos(self):
        return Vec2(self.x, self.y)
    
    @staticmethod
    def overlap(a1, a2, coords):
        for y in range(len(a1)):
            for x in range(len(a1[y])):
                if y >= coords.y and x >= coords.x and y - coords.y < len(a2) and x - coords.x < len(a2[0]):
                    a1[y][x] = a2[y- coords.y][x - coords.x]
        pass
    
    @staticmethod # Makes all inner-arrays the same size as the outer ones (Makes it square)
    def evenContent(arr, dummy):
        biggestIndex = 0
        for i in range(len(arr)):
            if len(arr[i]) > biggestIndex:
                biggestIndex = i
        for element in arr:
            element.extend(dummy * abs(len(element) - len(arr[biggestIndex])))

    @property
    def content(self):
        raise NotImplementedError

    @content.setter
    def content(self, other):
        self._content = deepcopy(other)


class Window(Interface, Component):
    
    def __init__(self, height, width, pos):
        self.height = height
        self.width = width
        self.components = []
        Component.__init__(self, pos)

    @property
    def content(self):
        self._content = []
        for y in range(self.height):
            self._content.append([])
            for x in range(self.width):
                if y == self.height-1 or x == self.width-1 or y == 0 or x == 0:
                    self._content[y].append("#")
                else:
                    self._content[y].append(" ")
        for component in self.components:
            Component.overlap(self._content, component.content, component.pos)
        return self._content
        

class Label(Component):
    
    def __init__(self, pos, text):
        super().__init__(pos)
        self.text = text

    @property
    def content(self):
        self._content = []

        for text in self.text.split("\n"):
            self._content.append(list(text))
        return self._content
    


class Button(Component):
    pass

if __name__ == "__main__":
    interface = Interface(10, 60)
    label = Label(Vec2(2,2), "haiya")
    window = Window(10, 60, Vec2(0,0))
    window += Label(Vec2(2,2), "This was")
    window += Label(Vec2(6,3), "Really hard")
    
    window += Label(Vec2(2,4), "To make")
    interface += window
    interface.display()

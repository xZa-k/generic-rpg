from display import Display
from block import *
from util import *
from entity import *
from math import floor, fmod
from time import time

# Groups collections of tiles together for more efficent loading of larger maps
class Chunk:
    
    # Chunk size, bound to class
    WIDTH = 39
    HEIGHT = 15

    # Defaults to fill tiles with a single type
    def __init__(self, tile: Tile) -> None:
        self.tiles: list[list[Tile]] = [[tile() for x in range(self.WIDTH)] for y in range(self.HEIGHT)]
        self.tiles[7][19] = Grass()
        self.tiles[0][0] = Grass()

    # Returns the array position from a given chunk delta
    @classmethod
    def chunkToArrayPos(cls, x: int, y: int) -> Vec2:
        return Vec2(x + cls.WIDTH//2, -(y - cls.HEIGHT//2))

    # Returns the array posistion, relative to center. Differs slightly from chunkToArrayPos
    @classmethod
    def centerToTopLeft(cls, x: int, y: int) -> Vec2:
        return Vec2(x - cls.WIDTH//2, -(y - cls.HEIGHT//2))

    # Returns a tile based on a given chunk delta
    def tile(self, x: int, y: int) -> Tile:
        centerCoords = self.chunkToArrayPos(x, y)
        return self.tiles[centerCoords.y][centerCoords.x]


class World:

    # Defines how many chunks are loaded at once
    REGION_SIZE = 3

    def __init__(self, player: Player, display: Display, viewX=0, viewY=0, ) -> None:

        # Initialise variables 
        self.viewPos: Vec2 = Vec2(viewX, viewY)
        self.chunks: list[list[Chunk]] = [[Chunk(Tree) for y in range(self.REGION_SIZE)] for x in range(self.REGION_SIZE)]
        self.entities: list[Entity] = [player]

        for i in range(40):
            self.entities.append(NPC(i, 3))
        
        # For testing
        self.chunks[1][0] = Chunk(StoneWall)
        self.chunks[0][1] = Chunk(StoneWall)
        self.chunks[1][1] = Chunk(Grass)
        self.chunks[0][0] = Chunk(Water)

        self.display: Display = display

    # Getters and setters for easier access to the view position
    @property
    def viewX(self):
        return self.viewPos.x

    @viewX.setter
    def viewX(self, other):
        self.viewPos.x = other

    @property
    def viewY(self):
        return self.viewPos.y

    @viewY.setter
    def viewY(self, other):
        self.viewPos.y = other
    
    # Returns the coordinates of the chunk a world position resides in
    @staticmethod
    def worldToChunk(x: int, y: int) -> Vec2:
        return Vec2(round(x / Chunk.WIDTH), round(y / Chunk.HEIGHT))
    
    # Returns the distance from the chunk center of a world coordinate
    @staticmethod
    def worldToChunkDelta(x: int, y: int) -> Vec2:
        return Vec2(x - Chunk.WIDTH*round(x / Chunk.WIDTH), (y - Chunk.HEIGHT*round(y / Chunk.HEIGHT)))

    # Wrapper for the addchr function that takes a tile argument instead
    def draw(self, x: int, y: int, tile: Tile):
        self.display.addchr(x, y, tile.symbol, tile.fg, tile.bg)

    # Returns the array position of a given word relative chunk coordinate
    @classmethod
    def worldToArrayPos(cls, x: int, y: int) -> Vec2:
        return Vec2(x + cls.REGION_SIZE//2, -(y - cls.REGION_SIZE//2))

    # Returns the chunk of a given world relative chunk coordinate
    def chunk(self, x: int, y: int) -> Chunk:
        pos = self.worldToArrayPos(x, y)
        return self.chunks[pos.y][pos.x]

    # Writes the tiles within the chunks and entities to the screen and refreshes it
    def refresh(self):
        
        startTime = time()
        # Gets the top left and bottom right of the current view area
        viewPoint1 = Chunk.centerToTopLeft(self.viewX, -self.viewY)
        viewPoint2 = Vec2(viewPoint1.x + Chunk.WIDTH, viewPoint1.y - Chunk.HEIGHT)

        # Iterate through every coordinate in that range
        for y in range(viewPoint1.y, viewPoint2.y, -1):
            for x in range(viewPoint1.x, viewPoint2.x):
                # Gets what chunk and the chunk delta coordinate
                chunkPos = self.worldToChunk(x, y)
                chunkDelta = self.worldToChunkDelta(x, y)

                # Gets the selected tile from the chunk it resides in
                chunk = self.chunk(chunkPos.x, chunkPos.y)
                tile = chunk.tile(chunkDelta.x, chunkDelta.y)
                
                # Calculates the tile position in the array
                tilePos = Chunk.chunkToArrayPos(int(fmod(x-self.viewX,Chunk.WIDTH/2)), int(fmod(y-self.viewY, Chunk.HEIGHT/2)))

                # Draws the tile to the screen
                self.draw(tilePos.x, tilePos.y, tile)

                entityStartTime = time()
                # Overwrites the tile with an entity if it has the same position
                for entity in self.entities:
                    if Vec2(x, y) == entity.pos:
                        self.draw(tilePos.x, tilePos.y, entity)
                self.display.addstr(Chunk.WIDTH+4, 4, str("%.10f" % (time() - entityStartTime)))
        
        self.display.addstr(Chunk.WIDTH+4, 3, str(1/(time() - startTime)))

        self.display.refresh()
    

if __name__ == "__main__":
    player = Player(0,0)
    display = Display()
    myWorld = World(player, display, viewX=0, viewY=0)
    
    # Start the game loop
    while True:

        # Refresh the screen and get input
        myWorld.refresh()
        key = display.window.getch()

        # Input handling
        if key == -1:
            display.addchr(0,0," ", "Black", "Red")
            continue
        display.addchr(0,0,chr(key), "Black", "Red")
        if chr(key) == "w":
            myWorld.viewY += 1
            player.y += 1
        elif chr(key) == "s":
            myWorld.viewY -= 1
            player.y -= 1
        elif chr(key) == "a":
            myWorld.viewX -= 1
            player.x -= 1
        elif chr(key) == "d":
            myWorld.viewX += 1
            player.x += 1

from util import *
from block import Tile

class Entity(Tile):
    def __init__(self, x, y, symbol="¬", fg="White"):
        self.pos = Vec2(x, y)
        super().__init__(symbol, fg=fg)

    def __str__(self):
        return self.symbol

    @property
    def x(self):
        return self.pos.x

    @x.setter
    def x(self, value):
        self.pos.x = value

    @property
    def y(self):
        return self.pos.y

    @y.setter
    def y(self, value):
        self.pos.y = value

class Player(Entity):
    
    def __init__(self, x, y, symbol="@", fg="White"):
        super().__init__(x, y, symbol=symbol, fg=fg)

class NPC(Entity):
    pass

class Enemy(Entity):
    pass

if __name__ == "__main__":
    ent = Entity(4, 4)
    print(ent.pos)
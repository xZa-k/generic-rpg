import curses
import json
from math import floor
import traceback
import time
import block
from pathlib import Path

class Display:

    def __init__(self, background = ("White", "Cyan")):
        # Initalise curses screen
        self.window = curses.initscr()
        curses.start_color()
        curses.noecho()
        curses.curs_set(0)

        self.window.timeout(0)
        # Initialise the colors
        self._colors = [] 
        self._colorPairs = []
        self._generateColorPairs()

        #Properties
        self._height, self._width = self.window.getmaxyx()
        self.title = "Zak's awesome game"

        self.background = background

    # Property getters and setters
    @property
    def background(self):
        return self._background

    @background.setter
    def background(self, other):
        self._background = other
        self.window.bkgd(0, self.colorAttr(self._background[0], self._background[1]))

    @property
    def height(self):
        self._height = self.window.getmaxyx()[0]
        return self._height

    @property
    def width(self):
        self._width = self.window.getmaxyx()[1]
        return self._width
        
    # Loads the color pallet and generates colors as well as all possible color pairs
    def _generateColorPairs(self):

        # Loads color pallet and initalises all the colors
        jsonColors = json.load(open(Path(__file__).parent / "pallet.json", "r"))
        index = 0
        for color in jsonColors:
            self._colors.append({
                "name":color["name"],
                "r":floor(color["rgb"]["r"]/255*1000), # Converts 0-255 rangne to  0-1000
                "g":floor(color["rgb"]["g"]/255*1000),
                "b":floor(color["rgb"]["b"]/255*1000),
                "id":color["colorId"]
            })
            curses.init_color(color["colorId"], self._colors[index]["r"], self._colors[index]["g"], self._colors[index]["b"])
            index += 1
        
        # From the color list, it generates all possible combinations
        index = 1
        for color1 in self._colors:
            for color2 in self._colors:
                self._colorPairs.append({
                    "id":index,
                    "fg":color1["name"],
                    "bg":color2["name"]
                })
                curses.init_pair(index, color1["id"], color2["id"])
                index += 1

    # Updates the screen with all the changes in the buffer
    def refresh(self):
        self.window.refresh()
        self.addstr(self.width//2-len(self.title)//2, 0, self.title)
    
    # Draws a character to the screen, refresh needs to be called for it to display
    def addchr(self, x: int, y: int, char: str, fg="", bg=""):
        
        if x < 0:
            x = self.width - x

        if y < 0:
            y = self.height - y

        if bg == "" or bg == "Transparent":
            attrBytes = self.window.inch(y, x)>>8
            bg = self._colorPairs[attrBytes-1]["bg"]
            

        if fg == "":
            fg = self.background[0]
        self.window.addch(y, x, char, self.colorAttr(fg, bg))

    # Same as addchr but can be a string rather than a char
    def addstr(self, x: int, y: int, string: str, fg="", bg=""):
        if bg == "":
            bg = self.background[1]
        if fg == "":
            fg = self.background[0]
        self.window.addstr(y, x, string, self.colorAttr(fg, bg))

    # Returns the id of a colour attribute based on a given fg and bg name
    def colorAttr(self, fg: str, bg: str) -> int:
        # Searches through the color array to find the correct id for the matching fg and bg
        for colorPair in self._colorPairs:
            if colorPair["fg"] == fg and colorPair["bg"] == bg:
                return curses.color_pair(colorPair["id"])


if __name__ == "__main__":
    display = Display()

    while True:
        try:
            display.refresh()
            # display.addstr(5, 5, " ", "Green", "Blue")
            try:
                pass
                # display.addchr(5, 10, chr(display.window.getch()), "White", "Black")
                # display.window.addstr(1, 1, str(display.width))
            except:
                pass
        except:
            curses.endwin()
            traceback.print_exc()
            quit()

class Vec2:
    
    def __init__(self, x, y):
        self.pos = [x, y]

    @property
    def x(self):
        return self.pos[0]

    @x.setter
    def x(self, value):
        self.pos[0] = value

    @property
    def y(self):
        return self.pos[1]

    @y.setter
    def y(self, value):
        self.pos[1] = value

    def __iadd__(self, other):
        self.x += other.x
        self.y += other.y
        return self

    def __add__(self, other):
        if not isinstance(other, Vec2):
            raise ValueError("Can only add vec2s together")
        return Vec2(self.x+other.x, self.y+other.y)
    
    def __getitem__(self, key):
        if key == 0:
            return self.x
        elif key == 1:
            return self.y
        else:
            raise IndexError("Index out of range")
    
    # TODO: Adapt this code to be more "property" friendly. Ugly rn
    def __setitem__(self, key, value):
        if key == 0:
            self.x = value
        elif key == 1:
            self.y = value
        else:
            raise IndexError("Index out of range")
    
    def __str__(self):
        return f"Vec2({self.x}, {self.y})"

    def __eq__(self, other):
        if isinstance(other, Vec2):
            if self.x == other.x and self.y == other.y:
                return True
            else:
                return False
        raise TypeError("Vec2 can only be compared to another Vec2")
if __name__ == "__main__":
    myVec = Vec2(5, 6)
    print(myVec)
    myVec.x = 10
    print(myVec)
